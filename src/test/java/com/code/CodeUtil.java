package com.code;

import com.mongo.generator.CodeGenerateFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cobert
 */

public class CodeUtil {
    public static void main(String[] args) {
        List<String> tables = initTable();

        for (String tableName : tables) {
            config(tableName, "cn.shell");
        }
    }

    public static List<String> initTable() {
        List<String> tables = new ArrayList<>();

        tables.add("Chat");

        return tables;
    }

    private static void config(String tableName, String packageName) {
        /** 此处修改成你的 表名 和 中文注释***/
        CodeGenerateFactory.codeGenerate(tableName, packageName);
    }

}
