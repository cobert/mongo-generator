package ${bussPackage}.service#if($!entityPackage).${entityPackage}#end;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import ${bussPackage}.dao#if($!entityPackage).${entityPackage}#end.${className}Dao;
import ${bussPackage}.service#if($!entityPackage).${entityPackage}#end.${className}Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *
 * <br>
 * <b>功能：</b>${className}Service<br>
 */
@Component
public class ${className}Service extends AbstractService {

    private final static Logger logger = Logger.getLogger(${className}Service.class);

	@Autowired
    private ${className}Repository ${lowerName}Repository;

    public ${className} save(${className} ${lowerName}) {
        return ${lowerName}Repository.save(${lowerName});
    }

    public List<${className}> findAll() {
        return ${lowerName}Repository.findAll();
    }

    public ${className} findOne(String id) {
        return ${lowerName}Repository.findOne(id);
    }

}
