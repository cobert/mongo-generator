package ${bussPackage}.repository#if($!entityPackage).${entityPackage}#end;

import com.base.dao.BaseDao;
/**
 * 
 * <br>
 * <b>功能：</b>${className}Repository<br>
 */
public interface ${className}Repository extends MongoRepository<${className}, String>, ${className}RepositoryCustom {
	
	
}
