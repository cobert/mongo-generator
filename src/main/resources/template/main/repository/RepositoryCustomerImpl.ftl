package ${bussPackage}.repository#if($!entityPackage).${entityPackage}#end;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
/**
 * 
 * <br>
 * <b>功能：</b>${className}RepositoryImpl<br>
 */

@Repository
public class ${className}RepositoryImpl implements ${className}RepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

	
}
