package com.mongo.generator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

/**
 *  @author cobert
 */
public class CodeGenerateFactory {
    private static final Log log = LogFactory.getLog(CodeGenerateFactory.class);
    private static String projectPath = getProjectPath();

    public static String FILE_SEPARATOR = System.getProperty("file.separator");

    public static String source_out = "sourceOut";

    public static void codeGenerate(String tableName, String packageName) {
//    String className = createBean.getTablesNameToClassName(tableName);
        String className = tableName;
        String lowerName = className.substring(0, 1).toLowerCase() + className.substring(1, className.length());

        String srcPath = projectPath + source_out + FILE_SEPARATOR;


        String pckPath = srcPath + packageName + FILE_SEPARATOR;


        String beanPath = "entity" + FILE_SEPARATOR + className + ".java";
        String repository = "repository" + FILE_SEPARATOR + className + "Repository.java";
        String repositoryCustomer = "repository" + FILE_SEPARATOR + className + "RepositoryCustom.java";
        String repositoryCustomerImpl = "repository" + FILE_SEPARATOR + className + "RepositoryImpl.java";
        String servicePath = "service" + FILE_SEPARATOR + className + "Service.java";
        String controllerPath = "controller" + FILE_SEPARATOR + className + "Controller.java";
//        String sqlMapperPath = "mapper" + FILE_SEPARATOR + entityPath + className + "Mapper.xml";

//        String webPath = projectPath + web_root_package + "\\view\\" + bussiPackageUrl + "\\";
//        webPath = webPath + entityPath;
//        String jspPath = lowerName + ".jsp";

        VelocityContext context = new VelocityContext();
        context.put("className", className);
        context.put("lowerName", lowerName);
        context.put("tableName", tableName);
        context.put("bussPackage", packageName);

        CommonPageParser.WriterPage(context, "template/main/entity/EntityTemplate.ftl", pckPath, beanPath);
        CommonPageParser.WriterPage(context, "template/main/repository/Repository.ftl", pckPath, repository);
        CommonPageParser.WriterPage(context, "template/main/repository/RepositoryCustomer.ftl", pckPath, repositoryCustomer);
        CommonPageParser.WriterPage(context, "template/main/repository/RepositoryCustomerImpl.ftl", pckPath, repositoryCustomerImpl);
        CommonPageParser.WriterPage(context, "template/main/service/ServiceImplTemplate.ftl", pckPath, servicePath);
        CommonPageParser.WriterPage(context, "template/main/controller/ControllerTemplate.ftl", pckPath, controllerPath);

        // CommonPageParser.WriterPage(context, "jspTemplate.ftl", webPath, jspPath);

        log.info("----------------------------\u4EE3\u7801\u751F\u6210\u5B8C\u6BD5---------------------------");
    }

    public static String getProjectPath() {
        String path = System.getProperty("user.dir").replace("\\", "/") + "/";
        return path;
    }
}